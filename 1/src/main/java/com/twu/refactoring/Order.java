package com.twu.refactoring;

import java.util.List;

public class Order {
    private String customerName;
    private String customerAddress;
    private List<LineItem> lineItems;

    public Order(String customerName, String customerAddress, List<LineItem> lineItems) {
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.lineItems = lineItems;
    }

    public void print(StringBuilder output) {
        output.append("======Printing Orders======\n");
        output.append(getCustomerName());
        output.append(getCustomerAddress());

        printItem(output);

        output.append("Sales Tax").append('\t').append(getTotSalesTx());
        output.append("Total Amount").append('\t').append(getTot());
    }

    private void printItem(StringBuilder output) {
        for (LineItem lineItem : getLineItems()) {
            output.append(lineItem.getDescription());
            output.append('\t');
            output.append(lineItem.getPrice());
            output.append('\t');
            output.append(lineItem.getQuantity());
            output.append('\t');
            output.append(lineItem.totalAmount());
            output.append('\n');
        }
    }

    private double getTotSalesTx() {
        double totSalesTx = 0d;
        for (LineItem lineItem : getLineItems()) {
            double salesTax = lineItem.totalAmount() * .10;
            totSalesTx += salesTax;
        }
        return totSalesTx;
    }

    private double getTot() {
        double tot = 0d;
        for (LineItem lineItem : getLineItems()) {
            double salesTax = lineItem.totalAmount() * .10;
            tot += lineItem.totalAmount() + salesTax;
        }
        return tot;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }
}
