package com.tuwc.luggage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CabinetTest {

    @Test
    void test_cabinet_deposit_luggage_return_ticket() {
        Cabinet cabinet = new Cabinet();
        Ticket ticket = cabinet.deposit(new Luggage());
        Assertions.assertNotNull(ticket);
    }
}
